<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DaftarController extends Controller
{
    public function daftar(){
        return view('form');
    }

    public function submit(Request $request){
       $fn = $request['fn'];
       $ln = $request['ln'];
       return view('selamat', compact('fn', 'ln'));
    }
}

?>
