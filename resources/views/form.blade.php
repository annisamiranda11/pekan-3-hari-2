<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REGISTER</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2> Sign Up Form </h2>

    <form action="/daftar" method="POST">
        @csrf
        <label> First Name </label><br><br>
        <input type="textarea" name="fn"><br><br>

        <label> Last Name </label><br><br>
        <input type="textarea" name="ln"><br><br>

        <label> Gender </label><br><br>
        <input type="radio" name="male">Male <br>
        <input type="radio" name="female">Female <br>
        <input type="radio" name="other">Other <br><br>

        <label> Nationality </label><br><br>
        <select>
            <option value="1"> Indonesian </option>
            <option value="2"> Malaysian </option>
            <option value="3"> Japanese </option>
            <option value="4"> American </option>
        </select> <br><br>

        <label> Language Spoken </label><br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>

        <label> Bio: </label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>

        <input type="submit">
    </form>
</body>
</html>